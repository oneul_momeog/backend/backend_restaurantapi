FROM openjdk:11-jdk
ENV LANG ko_KR.UTF8
ENV LC_ALL ko_KR.UTF8
RUN ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime
ARG JAR_DIR
COPY ${JAR_DIR} app.jar
EXPOSE 8081

ENTRYPOINT ["java","-jar","/app.jar", "--spring.profiles.active=prod"]