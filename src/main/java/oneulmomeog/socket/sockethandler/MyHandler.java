package oneulmomeog.socket.sockethandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.chat.dto.Message;
import oneulmomeog.socket.chat.dto.RoomPeopleCountDto;
import oneulmomeog.socket.chat.repository.ChatRepository;
import oneulmomeog.socket.chat.service.ChatService;
import oneulmomeog.socket.user.domain.User;
import oneulmomeog.socket.user.repository.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.*;

@Component
@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MyHandler extends TextWebSocketHandler {

    public static Map<Long, List<WebSocketSession>> roomListMap = new HashMap<>();

    private ObjectMapper objectMapper = new ObjectMapper();
    private final UserRepository userRepository;
    private final ChatService chatService;

    // TODO: 2022/11/21 현재는 userId, 닉네임 등을 메시지로 전달 받는 형태로 되어 있는데 이를 interceptor를 이용해서 가져오도록 수정하자.
    // TODO: 2022/11/24 추후 사용자가 접속하거나 나갈때 현재 방에 누가 있는지 명수 뿐만 아니라 닉네임까지 보내도록 수정
    @Override
    @Transactional
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String payload = message.getPayload();
        log.info("payload = {}", payload);
        log.info("message = {}", message);

        String query = session.getUri().getQuery();
        Long roomId = Objects.isNull(query) ? 1L : Long.parseLong(query.split("=")[1]);

        List<WebSocketSession> webSocketSessions = roomListMap.get(roomId);
        for (WebSocketSession webSocketSession : webSocketSessions) {
            webSocketSession.sendMessage(message);
        }

        Message messageObject = objectMapper.readValue(payload, Message.class);

        // 메시지 저장하기
        chatService.saveChat(
                messageObject.getRoomId(),
                messageObject.getUserId(),
                messageObject.getNickname(),
                messageObject.getContent(),
                messageObject.getMessageType()
        );

        User user = userRepository.findById(messageObject.getUserId());
        log.info("user data : userId = {}, userNickname = {}", user.getId(), user.getNickname());
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String query = session.getUri().getQuery();
        Long roomId = Objects.isNull(query) ? 1L : Long.parseLong(query.split("=")[1]);

        List<WebSocketSession> room;

        if (roomListMap.containsKey(roomId)) {
            room = roomListMap.get(roomId);
            room.add(session);
        } else {
            room = new ArrayList<>();
            room.add(session);
            roomListMap.put(roomId, room);
        }

        // 방 참가 인원 명수 전달
        sendRoomSize(room);

        log.info("query = {}, roomId = {}", query, roomId);
        log.info("{} 클라이언트 접속, chat room = {}, room size = {}", session.getId(), room.toString(), room.size());
    }


    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        String query = session.getUri().getQuery();
        Long roomId = Objects.isNull(query) ? 1L : Long.parseLong(query.split("=")[1]);

        List<WebSocketSession> webSocketSessions = roomListMap.get(roomId);
        webSocketSessions.remove(session);

        // 현재 룸 참가자 명수를 보내준다.
        sendRoomSize(webSocketSessions);

        log.info("{} 클라이언트 접속 해제, chat room = {}, room size = {}", session.getId(), webSocketSessions.toString(), webSocketSessions.size());

        // 룸에 사용자가 없으면 삭제한다.
        if (webSocketSessions.size() == 0) {
            roomListMap.remove(roomId);
        }
    }

    // 방 참가자가 몇명인지 보내주는 메서드
    private void sendRoomSize(List<WebSocketSession> webSocketSessions) throws IOException {
        RoomPeopleCountDto roomPeopleCountDto = new RoomPeopleCountDto(webSocketSessions.size());

        String json = objectMapper.writeValueAsString(roomPeopleCountDto);

        for (WebSocketSession webSocketSession : webSocketSessions) {
            webSocketSession.sendMessage(new TextMessage(json));
        }
    }
}
