package oneulmomeog.socket.sse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Component
@Slf4j
public class SseEmitters {

    private final Map<Long, SseEmitter> emitterMap = new HashMap<>();

    public void addSseEmitter(Long restaurantId, SseEmitter sseEmitter) {
        // SseEmitter 인스턴스 맵에 저장
        emitterMap.put(restaurantId, sseEmitter);
        log.info("new emitter add: {}", sseEmitter);
        log.info("emitterMap size = {}", emitterMap.size());
        sseEmitter.onCompletion(() -> {
            log.info("onCompletion callback");
            emitterMap.remove(restaurantId);
        });
        sseEmitter.onTimeout(() -> {
            log.info("onTimeout callback");
            sseEmitter.complete();
        });
    }

    public SseEmitter getEmitter(Long restaurantId) {
        SseEmitter sseEmitter = emitterMap.get(restaurantId);
        if (Objects.isNull(sseEmitter)) throw new IllegalArgumentException("접속되지 않은 상태입니다.");

        log.info("founded emitter = {}", sseEmitter);
        return sseEmitter;
    }

}
