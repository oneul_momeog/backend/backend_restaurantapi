package oneulmomeog.socket.chat.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.chat.dto.ChatRoomsDto;
import oneulmomeog.socket.chat.dto.ChatsDto;
import oneulmomeog.socket.chat.service.ChatService;
import oneulmomeog.socket.common.responsedto.SuccessResultWithData;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/chats")
public class ChatController {

    private final ChatService chatService;

    /**
     * 채팅룸 목록
     */
    @GetMapping
    public SuccessResultWithData<List<ChatRoomsDto>> getUserRooms(
            HttpServletRequest request
    ) {
        Long userId = Long.valueOf(Objects.toString(request.getAttribute("id")));

        log.info("getUserRooms controller param : userId = {}", userId);

        List<ChatRoomsDto> data = chatService.getUserChatRooms(userId);

        return new SuccessResultWithData<>("사용자 참가 룸 목록 전달 성공", data);
    }

    /**
     * 채팅 내역 목록
     */
    @GetMapping("/{roomId}/all")
    public SuccessResultWithData<List<ChatsDto>> getChats(
            @PathVariable Long roomId,
            @RequestParam(defaultValue = "0") Long chatId
    ) {
        log.info("getChats controller param : roomId = {}, chatId = {}", roomId, chatId);

        List<ChatsDto> data = chatService.getChats(roomId, chatId);
        return new SuccessResultWithData<>("채팅 내역 전달 성공", data);
    }


}


























