package oneulmomeog.socket.chat.repository;

import lombok.RequiredArgsConstructor;
import oneulmomeog.socket.chat.domain.Chat;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class ChatRepository {

    private final EntityManager em;

    public List<Chat> findChats(Long roomId, Long chatId) {
        List<Chat> chats = em.createQuery("select c from Chat c" +
                        " where c.room.id = :roomId" +
                        " and c.id < :chatId" +
                        " order by c.id", Chat.class)
                .setParameter("roomId", roomId)
                .setParameter("chatId", chatId)
                .setMaxResults(10)
                .getResultList();

        return chats;
    }

    public List<Chat> findChats(Long roomId) {
        List<Chat> chats = em.createQuery("select c from Chat c" +
                        " where c.room.id = :roomId" +
                        " order by c.id", Chat.class)
                .setParameter("roomId", roomId)
                .setMaxResults(10)
                .getResultList();

        return chats;
    }

    public void save(Chat chat) {
        em.persist(chat);
    }
}
