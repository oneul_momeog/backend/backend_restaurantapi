package oneulmomeog.socket.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class ChatsDto {

    private Long chatId;
    private Long userId;
    private String nickname;
    private String content;
    private String isImage;
}
