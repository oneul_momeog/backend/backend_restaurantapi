package oneulmomeog.socket.chat.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CeoMessageDto {
    @JsonProperty
    private Long userId = 0L;
    @JsonProperty
    private String nickname = "공지";
    @JsonProperty
    private String content;
}
