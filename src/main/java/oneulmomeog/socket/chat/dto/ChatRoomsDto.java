package oneulmomeog.socket.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ChatRoomsDto {

    private Long roomId;
    private Long restaurantId;
    private String roomName;
    private String restaurantName;
    private Integer maxPeople;
    private Integer currentPeople;
}