package oneulmomeog.socket.chat.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RoomPeopleCountDto {
    @JsonProperty
    private int currentPeople;
}
