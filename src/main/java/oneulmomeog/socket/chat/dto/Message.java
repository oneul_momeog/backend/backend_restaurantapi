package oneulmomeog.socket.chat.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class Message {

    private Long userId;
    private String nickname;
    private Long roomId;
    private String messageType;
    private String content;

    public Message() {}
}
