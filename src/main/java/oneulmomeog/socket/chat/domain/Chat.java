package oneulmomeog.socket.chat.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import oneulmomeog.socket.common.domain.Generate;
import oneulmomeog.socket.room.domain.Room;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@Entity
@DynamicInsert
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Chat extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "chat_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "room_id")
    private Room room;

    @Column(nullable = false)
    private Long userId;

    @Column(nullable = false)
    private String userNickname;

    @Column(nullable = false)
    private String content;

    @ColumnDefault("text")
    private String isImage;

    public Chat(Room room, Long userId, String userNickname, String content, String isImage) {
        this.room = room;
        this.userId = userId;
        this.userNickname = userNickname;
        this.content = content;
        this.isImage = isImage;
    }
}
