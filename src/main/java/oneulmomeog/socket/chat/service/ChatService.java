package oneulmomeog.socket.chat.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.chat.domain.Chat;
import oneulmomeog.socket.chat.dto.ChatRoomsDto;
import oneulmomeog.socket.chat.dto.ChatsDto;
import oneulmomeog.socket.chat.repository.ChatRepository;
import oneulmomeog.socket.room.domain.Room;
import oneulmomeog.socket.room.repository.RoomRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ChatService {

    private final RoomRepository roomRepository;
    private final ChatRepository chatRepository;
    private final EntityManager em;

    public List<ChatRoomsDto> getUserChatRooms(Long userId) {
        List<Room> rooms = roomRepository.findRoomsByUserId(userId);

        List<ChatRoomsDto> data = rooms.stream().map(room -> new ChatRoomsDto(
                room.getId(),
                room.getRestaurant().getId(),
                room.getRoomName(),
                room.getRestaurant().getRestaurantName(),
                room.getMaxPeople(),
                room.getCurrentPeople()
        )).collect(Collectors.toList());

        return data;
    }

    public List<ChatsDto> getChats(Long roomId, Long chatId) {
        List<Chat> chats;
        if (chatId.intValue() == 0) {
            log.info("채팅방 최초 접근");
            chats = chatRepository.findChats(roomId);
        } else {
            chats = chatRepository.findChats(roomId, chatId);
        }

        List<ChatsDto> data = chats.stream().map(chat -> new ChatsDto(
                chat.getId(),
                chat.getUserId(),
                chat.getUserNickname(),
                chat.getContent(),
                chat.getIsImage()
        )).collect(Collectors.toList());

        return data;
    }

    @Transactional
    public void saveChat(Long roomId, Long userId, String userNickname, String content, String isImage) {
        Room room = em.getReference(Room.class, roomId);
        Chat chat = new Chat(room, userId, userNickname, content, isImage);
        chatRepository.save(chat);
    }
}






















