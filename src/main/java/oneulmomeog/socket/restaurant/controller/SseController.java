package oneulmomeog.socket.restaurant.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.common.responsedto.SuccessResult;
import oneulmomeog.socket.restaurant.dto.RoomInfoDto;
import oneulmomeog.socket.sse.SseEmitters;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/ceo/sse")
@Slf4j
public class SseController {

    private final SseEmitters sseEmitters;

    @GetMapping(value = "/connect", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter getConnection(
            HttpServletRequest request
    ) throws IOException {
        Long restaurantId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        log.info("getConnection controller param : restaurantId = {}", restaurantId);

        SseEmitter sseEmitter = new SseEmitter(2 * 60 * 1000L);
        sseEmitter.send(SseEmitter.event()
                .name("connect")
                .data("connected!"));

        sseEmitters.addSseEmitter(restaurantId, sseEmitter);
        return sseEmitter;
    }

    @PostMapping("/room-ready")
    public SuccessResult count(
            @RequestBody RoomInfoDto roomInfoDto
            ) throws IOException {

        Long restaurantId = roomInfoDto.getRestaurantId();
        log.info("count controller param : roomInfoDto = {}", roomInfoDto);
        SseEmitter sseEmitter = sseEmitters.getEmitter(restaurantId);
        sseEmitter.send(SseEmitter.event()
                .name("ready")
                .data(roomInfoDto));

        return new SuccessResult("음식점에 룸 상태 전달 성공");
    }
}






















