package oneulmomeog.socket.restaurant.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.common.responsedto.SuccessResultWithData;
import oneulmomeog.socket.restaurant.dto.RestaurantManageDto;
import oneulmomeog.socket.restaurant.service.RestaurantManageService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/ceo")
@Slf4j
public class RestaurantManageController {

    private final RestaurantManageService restaurantManageService;

    /**
     * 음식점 관리 페이지 접속 시 관련 정보 요청
     */
    @GetMapping("/{restaurantId}/management")
    public SuccessResultWithData<RestaurantManageDto> getManageInfo(
            HttpServletRequest request,
            @PathVariable Long restaurantId
    ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!restaurantId.equals(authId)) throw new IllegalArgumentException("잘못된 접근입니다.");

        log.info("getManageInfo controller param : restaurantId = {}", restaurantId);
        RestaurantManageDto data = restaurantManageService.getRestaurantManageInfo(restaurantId);

        return new SuccessResultWithData<>("음식점 관리 정보 전달 성공", data);
    }

    /**
     * 음식점 관리 수정 요청
     */
    @GetMapping("/{restaurantId}/management/edit")
    public SuccessResultWithData<RestaurantManageDto> getManageInfoEdit(
            HttpServletRequest request,
            @PathVariable Long restaurantId
    ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!restaurantId.equals(authId)) throw new IllegalArgumentException("잘못된 접근입니다.");

        log.info("getManageInfo controller param : restaurantId = {}", restaurantId);
        RestaurantManageDto data = restaurantManageService.getRestaurantManageInfo(restaurantId);

        return new SuccessResultWithData<>("음식점 관리 정보 전달 성공", data);
    }

    /**
     * 음식점 관리 내용 작성
     */
    @PostMapping("/{restaurantId}/management/add")
    public SuccessResultWithData<RestaurantManageDto> addManageInfo(
            HttpServletRequest request,
            @PathVariable Long restaurantId,
            @Validated @RequestBody RestaurantManageDto restaurantManageDto
            ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!restaurantId.equals(authId)) throw new IllegalArgumentException("잘못된 접근입니다.");

        log.info("getManageInfo controller param : restaurantId = {}, requestbody = {}", restaurantId, restaurantManageDto.toString());

        RestaurantManageDto data = restaurantManageService.addRestaurantManageInfo(
                restaurantId,
                restaurantManageDto.getNotice(),
                restaurantManageDto.getEvent(),
                restaurantManageDto.getIngredientOrigin()
        );

        return new SuccessResultWithData<>("음식점 관리 작성 완료", data);
    }

    @PutMapping("/{restaurantId}/management/edit")
    public SuccessResultWithData<RestaurantManageDto> editManageInfo(
            HttpServletRequest request,
            @PathVariable Long restaurantId,
            @Validated @RequestBody RestaurantManageDto restaurantManageDto
    ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!restaurantId.equals(authId)) throw new IllegalArgumentException("잘못된 접근입니다.");

        log.info("getManageInfo controller param : restaurantId = {}, requestbody = {}", restaurantId, restaurantManageDto.toString());

        RestaurantManageDto data = restaurantManageService.addRestaurantManageInfo(
                restaurantId,
                restaurantManageDto.getNotice(),
                restaurantManageDto.getEvent(),
                restaurantManageDto.getIngredientOrigin()
        );

        return new SuccessResultWithData<>("음식점 관리 업데이트 완료", data);
    }
}




























