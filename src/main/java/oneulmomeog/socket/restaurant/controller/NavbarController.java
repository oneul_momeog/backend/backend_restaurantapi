package oneulmomeog.socket.restaurant.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.common.responsedto.SuccessResult;
import oneulmomeog.socket.restaurant.service.NavbarService;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/api/ceo")
@Slf4j
@RequiredArgsConstructor
public class NavbarController {

    private final NavbarService navbarService;

    @PatchMapping("/open")
    public SuccessResult changeOpen(
            HttpServletRequest request,
            @RequestBody Map<String, Boolean> body
    ) {
        Long restaurantId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        Boolean open = body.get("open");
        log.info("changeOpen controller param : restaurantId = {}, open = {}", restaurantId, open);

        navbarService.changeOpen(restaurantId, open);
        return new SuccessResult("음식점 영업 상태 변경 성공입니다.");
    }
}
