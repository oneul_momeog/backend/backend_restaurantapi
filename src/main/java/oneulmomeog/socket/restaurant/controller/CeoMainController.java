package oneulmomeog.socket.restaurant.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.chat.dto.CeoMessageDto;
import oneulmomeog.socket.common.responsedto.SuccessResult;
import oneulmomeog.socket.common.responsedto.SuccessResultWithData;
import oneulmomeog.socket.restaurant.dto.MainPageDto;
import oneulmomeog.socket.restaurant.service.CeoMainService;
import oneulmomeog.socket.sockethandler.MyHandler;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/ceo/main/restaurant")
@Slf4j
public class CeoMainController {

    private final CeoMainService ceoMainService;
    Map<Long, List<WebSocketSession>> roomListMap = MyHandler.roomListMap;
    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 음식점 메인페이지 주문 상태를 보여주는 화면
     */
    @GetMapping("/{restaurantId}")
    public SuccessResultWithData<MainPageDto> getRoomsInMain(
            HttpServletRequest request,
            @PathVariable Long restaurantId
    ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!authId.equals(restaurantId)) throw new IllegalArgumentException("잘못된 접근입니다.");

        log.info("getRoomsInMain controller param : restaurantId = {}", authId);

        MainPageDto data = ceoMainService.getRoomsInMain(authId);

        return new SuccessResultWithData<>("rooms 정보 전달 완료", data);
    }

    @Transactional
    @PatchMapping("/{restaurantId}/{status}")
    public SuccessResult updateToReady(
            HttpServletRequest request,
            @PathVariable Long restaurantId,
            @PathVariable String status,
            @RequestParam Long roomId
    ) throws IOException {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        log.info("updateToReady controller param : restaurantId = {}, status = {}, roomId = {}", authId, status, roomId);
        if (!authId.equals(restaurantId)) throw new IllegalArgumentException("잘못된 접근입니다.");

        ceoMainService.updateRoomStatus(authId, status, roomId);

        log.info("prev get list sessions");
        List<WebSocketSession> webSocketSessions = roomListMap.get(roomId);
        log.info("after get list sessions, webSocketSessions = {}", webSocketSessions);

        // 채팅방에 공지 메시지를 전달하고 테이블에 저장하는 서비스 로직
        Boolean isNotCancel = ceoMainService.saveAndSendMessage(restaurantId, status, roomId, webSocketSessions);

        if (isNotCancel == Boolean.FALSE) return new SuccessResult("주문 접수를 취소하였습니다.");
        return new SuccessResult("룸 상태 변경에 성공했습니다.");
    }


}




























