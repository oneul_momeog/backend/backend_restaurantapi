package oneulmomeog.socket.restaurant.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.chat.domain.Chat;
import oneulmomeog.socket.chat.dto.CeoMessageDto;
import oneulmomeog.socket.chat.repository.ChatRepository;
import oneulmomeog.socket.restaurant.domain.Restaurant;
import oneulmomeog.socket.restaurant.dto.MainPageDto;
import oneulmomeog.socket.restaurant.dto.RoomInfoDto;
import oneulmomeog.socket.restaurant.repository.RestaurantManageRepository;
import oneulmomeog.socket.room.domain.Room;
import oneulmomeog.socket.room.domain.RoomStatus;
import oneulmomeog.socket.room.repository.RoomRepository;
import oneulmomeog.socket.sockethandler.MyHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional(readOnly = true)
public class CeoMainService {

    private final RoomRepository roomRepository;
    private final EntityManager em;
    private final RestaurantManageRepository restaurantManageRepository;
    private final ChatRepository chatRepository;

    private ObjectMapper objectMapper = new ObjectMapper();

    // 메인페이지 룸 목록 전달
    public MainPageDto getRoomsInMain(Long restaurantId) {

        MainPageDto mainPageDto = new MainPageDto();

        List<Room> rooms = roomRepository.getRooms(restaurantId);
        rooms.stream().forEach(room -> {
            inputRoomInfoToMainPageDto(mainPageDto, room);
        });
        return mainPageDto;
    }

    // 접수 버튼 클릭 메서드
    @Transactional
    public void updateRoomStatus(Long restaurantId, String status, Long roomId) {
        Room room = roomRepository.findRoomById(restaurantId, roomId);
        if (Objects.isNull(room)) throw new IllegalArgumentException("해당하는 룸이 없습니다.");

        RoomStatus roomStatus = changeStringToEnum(status);
        room.updateRoomStatus(roomStatus);

        if (roomStatus.equals(RoomStatus.CANCEL)) {
            log.info("cancel room order and change room status");
        }
        log.info("change room status");
    }

    // 음식점 공지를 채팅방에 전달하고 테이블에 저장하는 메서드
    @Transactional
    public Boolean saveAndSendMessage(Long restaurantId, String status, Long roomId, List<WebSocketSession> webSocketSessions) throws IOException {
        log.info("saveAndSendMessage service param : restaurantId = {} webSocketSessions = {}", restaurantId, webSocketSessions);
        CeoMessageDto message = makeCeoMessage(status);
        log.info("message = {}", message);
        if (webSocketSessions != null) {
            log.info("send message");
            sendCeoMessage(webSocketSessions, message);
        }
        log.info("save message to chat table");

        // chat 테이블에 메시지 저장하기
        Room room = roomRepository.findRoomById(restaurantId, roomId);
        Restaurant restaurant = room.getRestaurant();
        Chat chat = new Chat(room, 0L, restaurant.getRestaurantName(), message.getContent(), "false");
        chatRepository.save(chat);

        // cancel 인 경우 room 삭제하기
        if (status.equals("cancel")) {
            log.info("cancel order and delete room");
            room.deleteRoom();
            return Boolean.FALSE;
        }

        // finish 인 경우 리뷰 메시지 전달하기
        if (status.equals("finish")) {
            CeoMessageDto ceoMessageDto = new CeoMessageDto();
            ceoMessageDto.setContent("리뷰를 작성해 주세요");
            sendCeoMessage(webSocketSessions, ceoMessageDto);

            // 메시지 저장
            Chat reviewChat = new Chat(room, 0L, restaurant.getRestaurantName(), ceoMessageDto.getContent(), "false");
            chatRepository.save(reviewChat);

            log.info("finish order and request reviewMessage");
        }
        return Boolean.TRUE;
    }



    // MainPageDto에 Room Entity를 넣어주는 메서드
    private void inputRoomInfoToMainPageDto(MainPageDto mainPageDto, Room room) {
        if (room.getStatus().equals(RoomStatus.READY)) {
            RoomInfoDto roomInfoDto = new RoomInfoDto(
                    room.getRestaurant().getId(),
                    room.getId(),
                    room.getExMenu(),
                    room.getTotalPrice(),
                    room.getCreatedAt().plusMinutes(room.getTimer()),
                    room.getRoomAddress().getFullAddress(),
                    room.getStatus()
            );
            mainPageDto.getReady().add(roomInfoDto);
        } else if (room.getStatus().equals(RoomStatus.RECEIVE)) {
            RoomInfoDto roomInfoDto = new RoomInfoDto(
                    room.getRestaurant().getId(),
                    room.getId(),
                    room.getExMenu(),
                    room.getTotalPrice(),
                    room.getCreatedAt().plusMinutes(room.getTimer()),
                    room.getRoomAddress().getFullAddress(),
                    room.getStatus()
            );
            mainPageDto.getReceive().add(roomInfoDto);
        } else {
            RoomInfoDto roomInfoDto = new RoomInfoDto(
                    room.getRestaurant().getId(),
                    room.getId(),
                    room.getExMenu(),
                    room.getTotalPrice(),
                    room.getCreatedAt().plusMinutes(room.getTimer()),
                    room.getRoomAddress().getFullAddress(),
                    room.getStatus()
            );
            mainPageDto.getDelivery().add(roomInfoDto);
        }
    }

    private RoomStatus changeStringToEnum(String status) {
        if (status.equals("receive")) {
            return RoomStatus.RECEIVE;
        } else if (status.equals("delivery")) {
            return RoomStatus.DELIVERY;
        } else if (status.equals("finish")) {
            return RoomStatus.FINISH;
        } else if (status.equals("cancel")) {
            return RoomStatus.CANCEL;
        } else {
            throw new IllegalArgumentException("접근할 수 없는 룸 상태입니다.");
        }
    }

    // 리뷰 메시지 전달 메서드
    private void sendReviewMessage(List<WebSocketSession> webSocketSessions) throws IOException {
        CeoMessageDto ceoMessageDto = new CeoMessageDto();
        ceoMessageDto.setContent("리뷰를 작성해 주세요");
        String json = objectMapper.writeValueAsString(ceoMessageDto);
        if (webSocketSessions != null) {
            for (WebSocketSession webSocketSession : webSocketSessions) {
                webSocketSession.sendMessage(new TextMessage(json));
            }
        }
    }

    // 공지 메시지 전달 메서드
    private void sendCeoMessage(List<WebSocketSession> webSocketSessions, CeoMessageDto ceoMessageDto) throws IOException {
        String json = objectMapper.writeValueAsString(ceoMessageDto);
        if (webSocketSessions != null) {
            for (WebSocketSession webSocketSession : webSocketSessions) {
                webSocketSession.sendMessage(new TextMessage(json));
            }
        }
    }

    // 메시지 만들어주는 메서드
    private CeoMessageDto makeCeoMessage(String status) {
        CeoMessageDto ceoMessageDto = new CeoMessageDto();
        if (status.equals("receive")) {
            ceoMessageDto.setContent("주문이 접수되었습니다.");
        } else if (status.equals("delivery")) {
            ceoMessageDto.setContent("배달이 시작되었습니다.");
        } else if (status.equals("finish")) {
            ceoMessageDto.setContent("배달이 완료되었습니다.");
        } else {
            ceoMessageDto.setContent("음식점 사정에 의해 주문이 취소되었습니다.");
        }
        return ceoMessageDto;
    }

}
