package oneulmomeog.socket.restaurant.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.restaurant.domain.Restaurant;
import oneulmomeog.socket.restaurant.dto.RestaurantManageDto;
import oneulmomeog.socket.restaurant.repository.RestaurantManageRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class RestaurantManageService {

    private final RestaurantManageRepository restaurantManageRepository;

    // 매장 관리 정보 전달 메서드
    public RestaurantManageDto getRestaurantManageInfo(Long restaurantId) {
        Restaurant restaurant = restaurantManageRepository.getRestaurantInfo(restaurantId);

        RestaurantManageDto data = new RestaurantManageDto(
                restaurant.getId(),
                restaurant.getNotice() == null ? "" : restaurant.getNotice(),
                restaurant.getEvent() == null ? "" : restaurant.getEvent(),
                restaurant.getIngredientsOrigin() == null ? "" : restaurant.getIngredientsOrigin());
        return data;
    }

    // 매장 관리 정보 작성
    @Transactional
    public RestaurantManageDto addRestaurantManageInfo(Long restaurantId, String notice, String event, String ingredientOrigin) {
        Restaurant updatedRestaurant = restaurantManageRepository.getRestaurantInfo(restaurantId);
        updatedRestaurant.updateRestaurantManageInfo(notice, event, ingredientOrigin);

        RestaurantManageDto data = new RestaurantManageDto(updatedRestaurant.getId(), updatedRestaurant.getNotice(), updatedRestaurant.getEvent(), updatedRestaurant.getIngredientsOrigin());
        return data;
    }
}
