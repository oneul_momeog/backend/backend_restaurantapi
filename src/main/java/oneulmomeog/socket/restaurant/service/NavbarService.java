package oneulmomeog.socket.restaurant.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.restaurant.domain.Restaurant;
import oneulmomeog.socket.restaurant.repository.RestaurantManageRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
@Slf4j
@RequiredArgsConstructor
public class NavbarService {

    private final RestaurantManageRepository restaurantManageRepository;

    @Transactional
    public void changeOpen(Long restaurantId, Boolean open) {
        Restaurant restaurant = restaurantManageRepository.getRestaurantInfo(restaurantId);
        restaurant.changeOpen(open);
    }
}
