package oneulmomeog.socket.restaurant.domain;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "restaurant_category")
@Getter
public class RestaurantCategory {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private Category category;

    protected RestaurantCategory() {}

    public RestaurantCategory(Restaurant restaurant, Category category) {
        this.restaurant = restaurant;
        this.category = category;
    }
}
