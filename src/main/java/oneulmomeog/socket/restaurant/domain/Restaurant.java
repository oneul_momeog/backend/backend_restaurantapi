package oneulmomeog.socket.restaurant.domain;

import lombok.Getter;
import oneulmomeog.socket.common.domain.Generate;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Entity
@Getter
public class Restaurant extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "restaurant_id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "restaurant_address_id")
    private RestaurantAddress restaurantAddress;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String restaurantName;

    private String restaurantImage;

    @ColumnDefault("false")
    private Boolean open;

    private String notice;
    private String event;
    private String ingredientsOrigin;
    private Integer deliveryFee;

    @Column(columnDefinition = "decimal(2,1) default 0")
    private Double meanRating;

    @Column(columnDefinition = "int default 0")
    private Integer reviewCount;

    /**
     * 비즈니스 메서드
     */
    public void updateRestaurantManageInfo(String notice, String event, String ingredientsOrigin) {
        this.notice = notice;
        this.event = event;
        this.ingredientsOrigin = ingredientsOrigin;
    }

    public void changeOpen(Boolean open) {
        if (open != this.open) {
            this.open = open;
        } else {
            throw new IllegalArgumentException("이미 " + open + "인 상태입니다.");
        }
    }
}
