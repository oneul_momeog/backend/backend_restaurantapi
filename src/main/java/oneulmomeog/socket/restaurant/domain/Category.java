package oneulmomeog.socket.restaurant.domain;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
public class Category {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long categoryId;

    @Column(nullable = false)
    private String categoryName;

    protected Category() {}

    public Category(String categoryName) {
        this.categoryName = categoryName;
    }
}
