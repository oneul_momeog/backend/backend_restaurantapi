package oneulmomeog.socket.restaurant.dto;

import lombok.Data;
import lombok.ToString;
import oneulmomeog.socket.room.domain.RoomStatus;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;

@Data
@ToString
public class RoomInfoDto {
    @Nullable
    private Long restaurantId;
    private Long roomId;
    private String exMenu;
    private Integer totalPrice;
    private LocalDateTime readyTime;
    private String deliveryLocation;
    private RoomStatus status;

    public RoomInfoDto(Long restaurantId, Long roomId, String exMenu, Integer totalPrice, LocalDateTime readyTime, String deliveryLocation, RoomStatus status) {
        this.restaurantId = restaurantId;
        this.roomId = roomId;
        this.exMenu = exMenu;
        this.totalPrice = totalPrice;
        this.readyTime = readyTime;
        this.deliveryLocation = deliveryLocation;
        this.status = status;
    }
}
