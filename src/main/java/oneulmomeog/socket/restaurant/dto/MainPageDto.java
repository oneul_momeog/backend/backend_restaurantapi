package oneulmomeog.socket.restaurant.dto;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class MainPageDto {
    private List<RoomInfoDto> ready = new ArrayList<>();
    private List<RoomInfoDto> receive = new ArrayList<>();
    private List<RoomInfoDto> delivery = new ArrayList<>();
}
