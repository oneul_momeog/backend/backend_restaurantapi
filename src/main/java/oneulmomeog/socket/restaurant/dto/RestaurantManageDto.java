package oneulmomeog.socket.restaurant.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class RestaurantManageDto {

    @Nullable
    private Long restaurantId;

    @NotNull
    private String notice;
    @NotNull
    private String event;
    @NotNull
    private String ingredientOrigin;
}
