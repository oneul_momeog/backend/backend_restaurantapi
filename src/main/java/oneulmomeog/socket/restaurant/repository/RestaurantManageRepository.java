package oneulmomeog.socket.restaurant.repository;

import lombok.RequiredArgsConstructor;
import oneulmomeog.socket.restaurant.domain.Restaurant;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
@RequiredArgsConstructor
public class RestaurantManageRepository {

    private final EntityManager em;

    public Restaurant getRestaurantInfo(Long restaurantId) {
        return em.find(Restaurant.class, restaurantId);
    }
}
