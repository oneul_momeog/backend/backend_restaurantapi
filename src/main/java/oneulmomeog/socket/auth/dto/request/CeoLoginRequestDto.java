package oneulmomeog.socket.auth.dto.request;

import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Getter
@ToString
public class CeoLoginRequestDto {
    @Email
    private String email;

    @NotNull
    private String password;
}
