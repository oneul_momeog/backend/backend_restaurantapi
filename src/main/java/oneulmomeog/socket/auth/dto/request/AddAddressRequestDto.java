package oneulmomeog.socket.auth.dto.request;

import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@ToString
public class AddAddressRequestDto {

    @NotNull
    private String zipcode;

    @NotNull
    private String normalAddress;

    @NotNull
    private String specificAddress;
}
