package oneulmomeog.socket.auth.dto.kakao;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class KakaoUserInfoResponseDto {

    private String id;
    private KakaoAccount kakao_account;
}
