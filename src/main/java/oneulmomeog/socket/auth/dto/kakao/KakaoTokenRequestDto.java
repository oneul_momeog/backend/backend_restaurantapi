package oneulmomeog.socket.auth.dto.kakao;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class KakaoTokenRequestDto {

    private String grant_type = "authorization_code";
    private String client_id;
    private String redirect_uri;
    private String code;

    public KakaoTokenRequestDto(String client_id, String redirect_uri, String code) {
        this.client_id = client_id;
        this.redirect_uri = redirect_uri;
        this.code = code;
    }
}
