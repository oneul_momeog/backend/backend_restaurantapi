package oneulmomeog.socket.auth.dto.kakao;

import lombok.Getter;

@Getter
public class Profile {
    private String nickname;
}
