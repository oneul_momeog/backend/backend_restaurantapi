package oneulmomeog.socket.auth.dto.kakao;

import lombok.Getter;

@Getter
public class KakaoAccount {
    private Profile profile;
}
