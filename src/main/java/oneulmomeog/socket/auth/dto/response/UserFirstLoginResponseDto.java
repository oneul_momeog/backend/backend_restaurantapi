package oneulmomeog.socket.auth.dto.response;

import lombok.Getter;
import oneulmomeog.socket.common.responsedto.SuccessResult;

@Getter
public class UserFirstLoginResponseDto extends SuccessResult {

    private UserFirstLoginDataResponseDto data;
    private Boolean isRegister;

    public UserFirstLoginResponseDto(String message, Boolean isRegister, UserFirstLoginDataResponseDto data) {
        super(message);
        this.isRegister = isRegister;
        this.data = data;
    }
}
