package oneulmomeog.socket.auth.dto.response;

import lombok.Getter;
import org.springframework.lang.Nullable;

@Getter
public class UserFirstLoginDataResponseDto {

    @Nullable
    private String jwt;
    private Long userId;
    private String nickname;
    private String zipcode;
    private String normalAddress;

    public void addJwt(String jwt) {
        this.jwt = jwt;
    }

    public UserFirstLoginDataResponseDto(Long userId, String nickname, String zipcode, String normalAddress) {
        this.userId = userId;
        this.nickname = nickname;
        this.zipcode = zipcode;
        this.normalAddress = normalAddress;
    }
}
