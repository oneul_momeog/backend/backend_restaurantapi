package oneulmomeog.socket.auth.dto.response;

import lombok.Getter;
import org.springframework.lang.Nullable;

@Getter
public class CeoRegisterAndLoginDataResponse {

    @Nullable
    private String jwt;

    private Long restaurantId;
    private String restaurantName;
    private String zipcode;
    private String branch;
    private Boolean open;

    /**
     * jwt 추가 메서드
     */
    public void addJwt(String jwt) {
        this.jwt = jwt;
    }

    public CeoRegisterAndLoginDataResponse(Long restaurantId, String restaurantName, String zipcode, String branch, Boolean open) {
        this.restaurantId = restaurantId;
        this.restaurantName = restaurantName;
        this.zipcode = zipcode;
        this.branch = branch;
        this.open = open;
    }
}
