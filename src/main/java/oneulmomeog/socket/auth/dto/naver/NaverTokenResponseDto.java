package oneulmomeog.socket.auth.dto.naver;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class NaverTokenResponseDto {

    private String access_token;
    private String refresh_token;
    private String token_type;
    private Integer expires_in;
    private String error;
    private String error_description;
}
