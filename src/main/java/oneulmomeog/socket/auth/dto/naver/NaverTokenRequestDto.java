package oneulmomeog.socket.auth.dto.naver;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NaverTokenRequestDto {

    private String grant_type;
    private String client_id;
    private String client_secret;
    private String code;
    private String state;
}
