package oneulmomeog.socket.auth.dto.naver;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class NaverUserInfoResponseDto {

    private String resultcode;
    private String message;
    private Response response;
}
