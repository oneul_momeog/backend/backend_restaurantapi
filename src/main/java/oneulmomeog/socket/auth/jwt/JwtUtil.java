package oneulmomeog.socket.auth.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.security.Key;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
public class JwtUtil {

    @Value("${token.secret}")
    private String secret;

    private ObjectMapper objectMapper = new ObjectMapper();

    public String makeJws(String type, Long id, String... infos) {
        Map<String, Object> payload = new HashMap<>();

        Key secretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secret));

        JwtBuilder builder = Jwts.builder();
        if (type.equals("user")) {
            builder.setSubject(type);
            payload.put("userId", id);
            payload.put("nickname", infos[0]);
            payload.put("zipcode", infos[1]);
        } else {
            builder.setSubject(type);
            payload.put("restaurantId", id);
            payload.put("zipcode", infos[0]);
        }
        String jws = builder.signWith(secretKey).setClaims(payload).compact();

        log.info("jws = {}", jws);
        return jws;
    }

    public Claims parseJwt(String jwt) {
        Key secretKey = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secret));

        Claims body = Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .build()
                .parseClaimsJws(jwt)
                .getBody();

        return body;
    }

    public String getToken(String bearerToken) {
        String token = bearerToken.split(" ")[1];

        return token;
    }
}
