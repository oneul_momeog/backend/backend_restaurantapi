package oneulmomeog.socket.exhandler.advice;

import io.jsonwebtoken.JwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.common.responsedto.ErrorResult;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;


@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ControllerAdvice {

    private final MessageSource messageSource;

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler
    public ErrorResult illegalExHandler(IllegalArgumentException e) {
        log.error("[IllegalArgumentException] ex = {}", e.getMessage());
        return new ErrorResult(406, e.getMessage());
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler
    public ErrorResult emailExHandler(MethodArgumentNotValidException e) {
        log.error("[MethodArgumentNotValidException] ex = {}", e.getBindingResult().getFieldError().getDefaultMessage());
        return new ErrorResult(406, e.getBindingResult().getFieldError().getDefaultMessage());
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler
    public ErrorResult jwtTokenHandler(JwtException e) {
        log.error("[jwtExceptionHandler] ex = {}", e.getMessage());
        return new ErrorResult(405, "만료된 토큰입니다.");
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler
    public ErrorResult missingParameterException(MissingServletRequestParameterException e) {
        log.error("[MissingServletRequestParameterException] ex = {}", e.getMessage());
        return new ErrorResult(405, "필요한 값이 없습니다.");
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler
    public ErrorResult notFoundException(NoHandlerFoundException e) {
        log.error("[NotFoundErrorException] ex = {}", e.getRequestURL());
        return new ErrorResult(404, "해당 api는 존재하지 않습니다.");
    }

//    @ResponseStatus(HttpStatus.OK)
//    @ExceptionHandler
//    public ErrorResult internalExHandler(RuntimeException e) {
//        log.error("[exceptionHandler] ex = {}", e.getStackTrace());
//        return new ErrorResult(500, "서버 내부 오류입니다.");
//    }

}
