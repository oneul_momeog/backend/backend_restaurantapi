package oneulmomeog.socket.review.domain;

import lombok.Getter;
import oneulmomeog.socket.common.domain.Generate;
import oneulmomeog.socket.restaurant.domain.Restaurant;
import oneulmomeog.socket.room.domain.Orders;
import oneulmomeog.socket.user.domain.User;

import javax.persistence.*;

@Entity
@Getter
public class Review extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Orders orders;

    @Column(nullable = false)
    private Double rating;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private String reviewImage;

    private String restaurantContent;

    protected Review() {}

    public Review(User user, Restaurant restaurant, Orders orders, Double rating, String content, String reviewImage) {
        this.user = user;
        this.restaurant = restaurant;
        this.orders = orders;
        this.rating = rating;
        this.content = content;
        this.reviewImage = reviewImage;
    }
}
