package oneulmomeog.socket.food.repository;

import lombok.RequiredArgsConstructor;
import oneulmomeog.socket.food.domain.Menu;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class MenuRepository {

    private final EntityManager em;

    public List<Menu> getMenus(List<Long> menuIds) {
        List<Menu> menuList = em.createQuery("select m from Menu m" +
                        " where m.id in :menuIds", Menu.class)
                .setParameter("menuIds", menuIds)
                .getResultList();

        return menuList;
    }

    public void save(Menu menu) {
        em.persist(menu);
    }

    public Menu findMenuByIdWithRestaurantId(Long restaurantId, Long menuId, Long groupId) {
        Menu menu = em.createQuery("select m from Menu m" +
                        " join fetch m.foodGroup f" +
                        " where m.id = :menuId and m.foodGroup.id = :groupId and f.restaurant.id = :restaurantId", Menu.class)
                .setParameter("menuId", menuId)
                .setParameter("groupId", groupId)
                .setParameter("restaurantId", restaurantId)
                .getResultStream()
                .findFirst()
                .orElse(null);

        return menu;
    }

    public List<Menu> findMenusByGroupId(Long groupId, Long restaurantId) {
        List<Menu> menus = em.createQuery("select m from Menu m" +
                        " join fetch m.foodGroup f" +
                        " where m.foodGroup.id = :groupId and f.restaurant.id = :restaurantId", Menu.class)
                .setParameter("groupId", groupId)
                .setParameter("restaurantId", restaurantId)
                .getResultList();

        return menus;
    }

    public void deleteMenu(Menu menu) {
        em.remove(menu);
    }
}
