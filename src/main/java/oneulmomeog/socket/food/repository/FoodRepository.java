package oneulmomeog.socket.food.repository;

import lombok.RequiredArgsConstructor;
import oneulmomeog.socket.food.domain.FoodGroup;
import oneulmomeog.socket.food.domain.Menu;
import oneulmomeog.socket.restaurant.domain.Restaurant;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class FoodRepository {

    private final EntityManager em;

    /**
     * 음식 메뉴 그룹을 보여주는 메서드
     */
    public List<FoodGroup> getGroups(Long restaurantId) {
        List<FoodGroup> groups = em.createQuery("select distinct f from FoodGroup f" +
                        " join f.restaurant r" +
                        " left join fetch f.menus m" +
                        " where r.id = :restaurantId", FoodGroup.class)
                .setParameter("restaurantId", restaurantId)
                .getResultList();

        return groups;
    }

    public FoodGroup save(Long restaurantId, String groupName) {
        Restaurant restaurant = em.getReference(Restaurant.class, restaurantId);
        FoodGroup foodGroup = new FoodGroup(restaurant, groupName);
        em.persist(foodGroup);

        return foodGroup;
    }

    public FoodGroup findByGroupId(Long restaurantId, Long groupId) {
        FoodGroup foodGroup = em.createQuery("select f from FoodGroup f" +
                        " where f.restaurant.id = :restaurantId" +
                        " and f.id = :groupId", FoodGroup.class)
                .setParameter("restaurantId", restaurantId)
                .setParameter("groupId", groupId)
                .getResultStream()
                .findFirst()
                .orElse(null);
        return foodGroup;
    }

    public FoodGroup findByGroupIdWithMenus(Long restaurantId, Long groupId) {
        FoodGroup foodGroup = em.createQuery("select distinct f from FoodGroup f" +
                        " join fetch f.menus m" +
                        " where f.restaurant.id = :restaurantId" +
                        " and f.id = :groupId", FoodGroup.class)
                .setParameter("restaurantId", restaurantId)
                .setParameter("groupId", groupId)
                .getResultStream()
                .findFirst()
                .orElse(null);

        return foodGroup;
    }

    public void deleteGroup(FoodGroup foodGroup) {
        em.remove(foodGroup);
    }



    /**
     * 그룹에 해당하는 메뉴를 보여주는 메서드
     */
//    public List<Menu> getMenus(Long restaurantId, Long groupId) {
//        List<Menu> menus = em.createQuery("select m from Menu m" +
//                        " join m.foodGroup f" +
//                        " join f.restaurant r" +
//                        " where r.id = :restaurantId and f.id = :groupId", Menu.class)
//                .setParameter("restaurantId", restaurantId)
//                .setParameter("groupId", groupId)
//                .getResultList();
//
//        return menus;
//    }



}
