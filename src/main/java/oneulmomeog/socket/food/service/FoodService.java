package oneulmomeog.socket.food.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.common.file.FileStore;
import oneulmomeog.socket.food.domain.FoodGroup;
import oneulmomeog.socket.food.domain.Menu;
import oneulmomeog.socket.food.dto.GroupDto;
import oneulmomeog.socket.food.repository.FoodRepository;
import oneulmomeog.socket.food.repository.MenuRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class FoodService {

    private final FoodRepository foodRepository;
    private final MenuRepository menuRepository;
    private final FileStore fileStore;

    /**
     * 메뉴 그룹을 보여주는 메서드
     */
    public List<GroupDto> getGroups(Long restaurantId) {
        List<FoodGroup> groups = foodRepository.getGroups(restaurantId);

        List<GroupDto> data = groups.stream().map(foodGroup -> new GroupDto(foodGroup))
                .collect(Collectors.toList());

        return data;
    }

    /**
     * 그룹 추가하는 메서드
     */
    @Transactional
    public GroupDto addGroup(Long restaurantId, String groupName) {
        FoodGroup foodGroup = foodRepository.save(restaurantId, groupName);

        GroupDto data = new GroupDto(foodGroup);
        return data;
    }

    /**
     * 그룹 수정 요청 메서드
     */
    public GroupDto findByGroupId(Long restaurantId, Long groupId) {
        FoodGroup foodGroup = foodRepository.findByGroupId(restaurantId, groupId);
        if (Objects.isNull(foodGroup)) throw new IllegalArgumentException("해당 그룹이 존재하지 않습니다.");

        GroupDto groupDto = new GroupDto(foodGroup.getId(), foodGroup.getGroupName());
        return groupDto;
    }

    /**
     * 그룹 수정 메서드
     */
    @Transactional
    public GroupDto updateGroup(Long restaurantId, Long groupId, String groupName) {
        FoodGroup foodGroup = foodRepository.findByGroupId(restaurantId, groupId);
        if (Objects.isNull(foodGroup)) throw new IllegalArgumentException("해당 그룹이 존재하지 않습니다.");

        foodGroup.updateGroupName(groupName);
        GroupDto groupDto = new GroupDto(foodGroup.getId(), foodGroup.getGroupName());
        return groupDto;
    }

    /**
     * 그룹 삭제 메서드
     */
    @Transactional
    public void deleteGroup(Long restaurantId, Long groupId) {
        FoodGroup foodGroup = foodRepository.findByGroupIdWithMenus(restaurantId, groupId);
        if (Objects.isNull(foodGroup)) throw new IllegalArgumentException("해당 그룹이 존재하지 않습니다.");

        List<Menu> menus = foodGroup.getMenus();
        menus.stream().forEach(menu -> {
            menuRepository.deleteMenu(menu);
            fileStore.deleteFile(menu.getMenuImage());
        });

        foodRepository.deleteGroup(foodGroup);
    }
}














