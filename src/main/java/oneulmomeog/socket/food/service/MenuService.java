package oneulmomeog.socket.food.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.common.file.FileStore;
import oneulmomeog.socket.food.domain.FoodGroup;
import oneulmomeog.socket.food.domain.Menu;
import oneulmomeog.socket.food.dto.MenuDto;
import oneulmomeog.socket.food.repository.FoodRepository;
import oneulmomeog.socket.food.repository.MenuRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional(readOnly = true)
public class MenuService {

    private final FileStore fileStore;
    private final FoodRepository foodRepository;
    private final MenuRepository menuRepository;

    // 메뉴 추가
    @Transactional
    public MenuDto addMenu(
            Long restaurantId,
            Long groupId,
            String menuName,
            String description,
            Integer price,
            String ingredients,
            MultipartFile menuImage
    ) throws IOException {
        String filePath = fileStore.storeFile(menuImage);
        log.info("filePath = {}", filePath);
        FoodGroup foodGroup = foodRepository.findByGroupId(restaurantId, groupId);
        if (Objects.isNull(foodGroup)) throw new IllegalArgumentException("해당 음식 그룹이 존재하지 않습니다.");
        Menu menu = new Menu(foodGroup, menuName, description, price, filePath, ingredients);
        menuRepository.save(menu);

        return new MenuDto(menu.getId(), menu.getMenuName(), menu.getDescription(), menu.getPrice(), menu.getMenuImage(), menu.getIngredients(), menu.getSoldOut());
    }

    // 메뉴 수정 데이터 요청
    public MenuDto getMenuInfo(Long restaurantId, Long groupId, Long menuId) {
        Menu menu = menuRepository.findMenuByIdWithRestaurantId(restaurantId, menuId, groupId);
        if (Objects.isNull(menu)) throw new IllegalArgumentException("해당하는 메뉴가 없습니다.");

        return new MenuDto(menu.getId(), menu.getMenuName(), menu.getDescription(), menu.getPrice(), menu.getMenuImage(), menu.getIngredients(), menu.getSoldOut());
    }

    // 메뉴 수정
    @Transactional
    public MenuDto editMenu(
            Long restaurantId,
            Long groupId,
            Long menuId,
            String menuName,
            String description,
            Integer price,
            String ingredients,
            MultipartFile menuImage
    ) throws IOException {
        String filePath = fileStore.storeFile(menuImage);
        log.info("filePath = {}", filePath);
        FoodGroup foodGroup = foodRepository.findByGroupId(restaurantId, groupId);
        if (Objects.isNull(foodGroup)) throw new IllegalArgumentException("해당 음식 그룹이 존재하지 않습니다.");
        Menu findedMenu = menuRepository.findMenuByIdWithRestaurantId(restaurantId, menuId, groupId);

        // 기존 파일 삭제
        fileStore.deleteFile(findedMenu.getMenuImage());

        findedMenu.updateMenu(menuName, description, price, filePath, ingredients);
        return new MenuDto(findedMenu.getId(), findedMenu.getMenuName(), findedMenu.getDescription(), findedMenu.getPrice(), findedMenu.getMenuImage(), findedMenu.getIngredients(), findedMenu.getSoldOut());
    }

    @Transactional
    public void deleteMenu(Long restaurantId, Long groupId, Long menuId) {
        Menu menu = menuRepository.findMenuByIdWithRestaurantId(restaurantId, menuId, groupId);

        // 기존 파일 삭제
        fileStore.deleteFile(menu.getMenuImage());
        menuRepository.deleteMenu(menu);
    }

    @Transactional
    public void changeSoldOut(Long restaurantId, Long groupId, Long menuId, Boolean soldOut) {
        Menu menu = menuRepository.findMenuByIdWithRestaurantId(restaurantId, menuId, groupId);
        menu.updateSoldOut(soldOut);

        return;
    }

}