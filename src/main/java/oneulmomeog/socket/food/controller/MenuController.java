package oneulmomeog.socket.food.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.common.responsedto.SuccessResult;
import oneulmomeog.socket.common.responsedto.SuccessResultWithData;
import oneulmomeog.socket.food.dto.ChangeSoldOutRequestDto;
import oneulmomeog.socket.food.dto.MenuDto;
import oneulmomeog.socket.food.service.MenuService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/ceo")
@Slf4j
public class MenuController {

    private final MenuService menuService;

    /**
     * 메뉴 추가 컨트롤러
     */
    @PostMapping("/menus/menu/add")
    public SuccessResultWithData<MenuDto> addMenu(
            HttpServletRequest request,
            @RequestParam Long restaurantId,
            @RequestParam Long groupId,
            @RequestParam String menuName,
            @RequestParam String description,
            @RequestParam Integer price,
            @RequestParam String ingredients,
            @RequestParam MultipartFile menuImage
    ) throws IOException {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!authId.equals(restaurantId)) throw new IllegalArgumentException("유효하지 않은 접근입니다.");

        log.info("addMenu controller param : restaurantId = {}, groupId = {}" +
                ", menuName = {}, description = {}, price = {}, ingredients = {}, menuImage = {}",
                authId, groupId, menuName, description, price, ingredients, menuImage);

        MenuDto data = menuService.addMenu(authId, groupId, menuName, description, price, ingredients, menuImage);
        return new SuccessResultWithData<>("메뉴 추가 완료", data);
    }

    /**
     * 메뉴 수정 요청
     */
    @GetMapping("/menus/menu/edit")
    public SuccessResultWithData<MenuDto> getMenuInfo(
            HttpServletRequest request,
            @RequestParam Long restaurantId,
            @RequestParam Long groupId,
            @RequestParam Long menuId
    ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!authId.equals(restaurantId)) throw new IllegalArgumentException("유효하지 않은 접근입니다.");

        log.info("getMenuInfo controller param : restaurantId = {}, groupId = {}, menuId = {}", authId, groupId, menuId);
        MenuDto data = menuService.getMenuInfo(authId, groupId, menuId);

        return new SuccessResultWithData<>("메뉴 정보 전달 완료", data);
    }

    /**
     * 메뉴 수정
     */
    @PutMapping("/menus/menu/edit")
    public SuccessResultWithData<MenuDto> updateMenuInfo(
            HttpServletRequest request,
            @RequestParam Long restaurantId,
            @RequestParam Long groupId,
            @RequestParam Long menuId,
            @RequestParam String menuName,
            @RequestParam String description,
            @RequestParam Integer price,
            @RequestParam String ingredients,
            @RequestParam MultipartFile menuImage
    ) throws IOException {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!authId.equals(restaurantId)) throw new IllegalArgumentException("유효하지 않은 접근입니다.");

        log.info("addMenu controller param : restaurantId = {}, groupId = {}, menuId = {}" +
                        ", menuName = {}, description = {}, price = {}, ingredients = {}, menuImage = {}",
                authId, groupId, menuId, menuName, description, price, ingredients, menuImage);

        MenuDto data = menuService.editMenu(restaurantId, groupId, menuId, menuName, description, price, ingredients, menuImage);
        return new SuccessResultWithData<>("메뉴 정보 수정 완료", data);
    }

    /**
     * 메뉴 삭제
     */
    @DeleteMapping("/menus/menu/delete")
    public SuccessResult deleteMenu(
            HttpServletRequest request,
            @RequestParam Long menuId,
            @RequestParam Long groupId
    ) {
        Long restaurantId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        log.info("deleteMenu controller param : restaurantId = {}, menuId = {}, groupId = {}", restaurantId, menuId, groupId);

        menuService.deleteMenu(restaurantId, groupId, menuId);
        return new SuccessResult("메뉴 삭제 성공");
    }

    /**
     * 메뉴 품절 여부 수정
     */
    @PatchMapping("/menus/menu/sold-out/edit")
    public SuccessResult updateSoldOut(
            HttpServletRequest request,
            @Validated @RequestBody ChangeSoldOutRequestDto changeSoldOutRequestDto
    ) {
        Long restaurantId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!restaurantId.equals(changeSoldOutRequestDto.getRestaurantId()))
            throw new IllegalArgumentException("잘못된 접근입니다.");

        menuService.changeSoldOut(
                changeSoldOutRequestDto.getRestaurantId(),
                changeSoldOutRequestDto.getGroupId(),
                changeSoldOutRequestDto.getMenuId(),
                changeSoldOutRequestDto.getSoldOut()
        );

        return new SuccessResult("품절 여부 변경 완료입니다.");
    }
}




























