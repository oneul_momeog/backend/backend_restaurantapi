package oneulmomeog.socket.food.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.common.responsedto.SuccessResult;
import oneulmomeog.socket.common.responsedto.SuccessResultWithData;
import oneulmomeog.socket.food.dto.GroupDto;
import oneulmomeog.socket.food.dto.GroupRequestDto;
import oneulmomeog.socket.food.service.FoodService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/ceo")
@RequiredArgsConstructor
@Slf4j
public class FoodController {

    private final FoodService foodService;

    /**
     * 음식점 메뉴 그룹을 가져오는 컨트롤러
     */
    @GetMapping("/{restaurantId}/menus/management")
    public SuccessResultWithData<List<GroupDto>> getGroups(
            @PathVariable Long restaurantId,
            HttpServletRequest request
    ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!restaurantId.equals(authId)) throw new IllegalArgumentException("잘못된 접근입니다.");
        log.info("getGroups controller param : restaurantId = {}", restaurantId);
        List<GroupDto> data = foodService.getGroups(restaurantId);

        return new SuccessResultWithData<>("메뉴 그룹 전송 완료", data);
    }

    /**
     * 그룹 추가 컨트롤러
     */
    @PostMapping("/menus/group/add")
    public SuccessResultWithData<GroupDto> addGroup(
            HttpServletRequest request,
            @Validated @RequestBody GroupRequestDto requestBody
    ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        if (!authId.equals(requestBody.getRestaurantId())) throw new IllegalArgumentException("잘못된 접근입니다.");

        log.info("addGroup controller param : restaurantId = {}, requestBody = {}", authId, requestBody);

        GroupDto data = foodService.addGroup(authId, requestBody.getGroupName());

        return new SuccessResultWithData<>("그룹 추가 완료", data);
    }

    /**
     * 그룹 수정 요청 컨트롤러
     */
    @GetMapping("/menus/group/{groupId}/edit")
    public SuccessResultWithData<GroupDto> getEditGroup(
            HttpServletRequest request,
            @PathVariable Long groupId
    ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        log.info("getEditGroup controller param : restaurantId = {}, groupId = {}", authId, groupId);

        GroupDto data = foodService.findByGroupId(authId, groupId);
        return new SuccessResultWithData<>("그룹 전달 완료", data);
    }

    /**
     * 그룹 수정
     */
    @PutMapping("/menus/group/{groupId}/edit")
    public SuccessResultWithData<GroupDto> editGroup(
            HttpServletRequest request,
            @PathVariable Long groupId,
            @RequestBody GroupRequestDto requestBody
    ) {
        Long authId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        log.info("editGroup controller param : restaurantId = {}, groupId = {}, groupName = {}", authId, groupId, requestBody.getGroupName());

        GroupDto data = foodService.updateGroup(authId, groupId, requestBody.getGroupName());
        return new SuccessResultWithData<>("그룹 수정 완료", data);
    }

    /**
     * 그룹 삭제
     */
    @DeleteMapping("/menus/group/{groupId}/delete")
    public SuccessResult deleteGroup(
            HttpServletRequest request,
            @PathVariable Long groupId
    ) {
        Long restaurantId = Long.valueOf(Objects.toString(request.getAttribute("restaurantId")));
        log.info("deleteGroup controller param : restaurantId = {}, groupId = {}", restaurantId, groupId);

        foodService.deleteGroup(restaurantId, groupId);
        return new SuccessResult("메뉴 그룹 삭제 완료");
    }
}

























