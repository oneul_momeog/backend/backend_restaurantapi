package oneulmomeog.socket.food.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ChangeSoldOutRequestDto {

    @NotNull
    private Long restaurantId;
    @NotNull
    private Long groupId;
    @NotNull
    private Long menuId;
    @NotNull
    private Boolean soldOut;
}
