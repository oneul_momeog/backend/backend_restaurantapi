package oneulmomeog.socket.food.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import oneulmomeog.socket.food.domain.FoodGroup;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public class GroupDto {

    private Long groupId;
    private String groupName;
    @Nullable
    private List<MenuDto> menus;

    public GroupDto(Long groupId, String groupName) {
        this.groupId = groupId;
        this.groupName = groupName;
    }

    public GroupDto(FoodGroup group) {
        this.groupId = group.getId();
        this.groupName = group.getGroupName();
        this.menus = group.getMenus().stream()
                .map(menu -> new MenuDto(
                        menu.getId(),
                        menu.getMenuName(),
                        menu.getDescription(),
                        menu.getPrice(),
                        menu.getMenuImage(),
                        menu.getIngredients(),
                        menu.getSoldOut()
                )).collect(Collectors.toList());
    }
}
