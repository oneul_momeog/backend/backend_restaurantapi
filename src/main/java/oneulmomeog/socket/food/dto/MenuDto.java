package oneulmomeog.socket.food.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class MenuDto {

    private Long menuId;
    private String menuName;
    private String description;
    private int price;
    private String menuImage;
    private String ingredients;
    private Boolean soldOut;
}
