package oneulmomeog.socket.food.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class GroupRequestDto {

    @NotNull
    private Long restaurantId;
    @NotNull
    private String groupName;
}
