package oneulmomeog.socket.food.dto;

import lombok.Data;

@Data
public class MenuRequestDto {

    private Long restaurantId;
    private Long groupId;
    private String menuName;
    private String description;
    private int price;
    private String menuImage;
}
