package oneulmomeog.socket.food.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import oneulmomeog.socket.common.domain.Generate;
import oneulmomeog.socket.restaurant.domain.Restaurant;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FoodGroup  extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "groupd_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @Column(nullable = false)
    private String groupName;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "foodGroup")
    private List<Menu> menus = new ArrayList<>();

    public FoodGroup(Restaurant restaurant, String groupName) {
        this.restaurant = restaurant;
        this.groupName = groupName;
    }

    public void updateGroupName(String groupName) {
        this.groupName = groupName;
    }

}
