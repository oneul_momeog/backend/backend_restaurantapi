package oneulmomeog.socket.food.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import oneulmomeog.socket.common.domain.Generate;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@Entity
@DynamicInsert
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Menu extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "menu_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    private FoodGroup foodGroup;

    @Column(nullable = false)
    private String menuName;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private int price;

    @Column(nullable = false)
    private String menuImage;

    @Column(nullable = false)
    private String ingredients;

    @ColumnDefault("false")
    private Boolean soldOut;

    public Menu(FoodGroup foodGroup, String menuName, String description, int price, String menuImage, String ingredients) {
        this.foodGroup = foodGroup;
        this.menuName = menuName;
        this.description = description;
        this.price = price;
        this.menuImage = menuImage;
        this.ingredients = ingredients;
    }

    public Menu updateMenu(String menuName, String description, int price, String menuImage, String ingredients) {
        this.menuName = menuName;
        this.description = description;
        this.price = price;
        this.menuImage = menuImage;
        this.ingredients = ingredients;
        return this;
    }

    public void updateSoldOut(Boolean soldOut) {
        if (soldOut != this.soldOut) {
            this.soldOut = soldOut;
        } else {
            throw new IllegalArgumentException("이미 " + soldOut + " 인 상태입니다.");
        }
    }
}
