package oneulmomeog.socket.config;

import lombok.RequiredArgsConstructor;
import oneulmomeog.socket.common.interceptor.CeoLoginInterceptor;
import oneulmomeog.socket.common.interceptor.UserLoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@PropertySources({@PropertySource("classpath:properties/env.properties")})
@RequiredArgsConstructor
public class WebConfig implements WebMvcConfigurer {

    private final UserLoginInterceptor userLoginInterceptor;
    private final CeoLoginInterceptor ceoLoginInterceptor;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("*")
                .maxAge(1800);
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(userLoginInterceptor)
                .order(1)
                .addPathPatterns("/api/auth/oauth/address")
                .addPathPatterns("/api/main/**")
                .addPathPatterns("/api/reviews/**")
                .addPathPatterns("/api/restaurants/**")
                .addPathPatterns("/api/chats/**")
                .excludePathPatterns("/", "/css/**", "/*.icon");

        registry.addInterceptor(ceoLoginInterceptor)
                .order(2)
                .addPathPatterns("/api/ceo/**")
                .excludePathPatterns("/", "/css/**", "/*.icon", "/api/ceo/sse/room-ready");
    }
}
