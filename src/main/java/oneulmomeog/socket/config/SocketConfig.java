package oneulmomeog.socket.config;

import lombok.RequiredArgsConstructor;
import oneulmomeog.socket.sockethandler.MyHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.*;

@Configuration
@EnableWebSocket
@RequiredArgsConstructor
public class SocketConfig implements WebSocketConfigurer {

    private final MyHandler myHandler;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(myHandler, "ws/chat")
                .setAllowedOriginPatterns("*");
    }
}
