package oneulmomeog.socket.common.domain;

import lombok.Getter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
@Getter
public abstract class Generate {

    @CreatedDate
    @Column(updatable = false, columnDefinition = "datetime default now()")
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(columnDefinition = "datetime default now()")
    private LocalDateTime updatedAt;

    @Column(columnDefinition = "Boolean default false")
    private Boolean deleted = Boolean.FALSE;

    // 비즈니스 메서드
    public void delete() {
        this.deleted = Boolean.TRUE;
    }
}
