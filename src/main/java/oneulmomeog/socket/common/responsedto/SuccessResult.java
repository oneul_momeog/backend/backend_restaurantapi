package oneulmomeog.socket.common.responsedto;

import lombok.Getter;

@Getter
public class SuccessResult {

    private Boolean success = Boolean.TRUE;
    private String message;

    public SuccessResult(String message) {
        this.message = message;
    }

    public SuccessResult() {

    }
}
