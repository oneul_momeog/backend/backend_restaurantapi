package oneulmomeog.socket.common.responsedto;

import lombok.Getter;

@Getter
public class SuccessResultWithData<T> {

    private Boolean success = Boolean.TRUE;
    private String message;
    private T data;

    public SuccessResultWithData(String message, T data) {
        this.message = message;
        this.data = data;
    }
}
