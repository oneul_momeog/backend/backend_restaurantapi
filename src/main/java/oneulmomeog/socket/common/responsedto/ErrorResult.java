package oneulmomeog.socket.common.responsedto;

import lombok.Getter;

@Getter
public class ErrorResult {

    private final Boolean success = Boolean.FALSE;
    private int code;
    private String message;

    public ErrorResult(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
