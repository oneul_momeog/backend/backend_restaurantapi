package oneulmomeog.socket.common.interceptor;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.auth.jwt.JwtUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Component
public class CeoLoginInterceptor implements HandlerInterceptor {

    private final JwtUtil jwtUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("[Interceptor] CeoLoginInterceptor");
        String bearerJwt = request.getHeader(HttpHeaders.AUTHORIZATION);
        log.info("BearerJwt = {}", bearerJwt);

        if (isPreflightRequest(request)) {
            return true;
        }

        String jwt = jwtUtil.getToken(bearerJwt);
        log.info("getToken = {}", jwt);

        Claims parseJwt = null;
        try {
            parseJwt = jwtUtil.parseJwt(jwt);
            log.info("parseJwt = {}", parseJwt);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        String restaurantId = String.valueOf(parseJwt.get("restaurantId"));
        log.info("restaurantId = {}", restaurantId);

        request.setAttribute("restaurantId", restaurantId);

//        request.setAttribute("restaurantId", 1);
        return Boolean.TRUE;
    }

    private boolean isPreflightRequest(HttpServletRequest request) {
        return isOptions(request) && hasHeaders(request) && hasMethod(request) && hasOrigin(request);
    }

    private boolean isOptions(HttpServletRequest request) {
        return request.getMethod().equalsIgnoreCase(HttpMethod.OPTIONS.toString());
    }

    private boolean hasHeaders(HttpServletRequest request) {
        return Objects.nonNull(request.getHeader("Access-Control-Request-Headers"));
    }

    private boolean hasMethod(HttpServletRequest request) {
        return Objects.nonNull(request.getHeader("Access-Control-Request-Method"));
    }

    private boolean hasOrigin(HttpServletRequest request) {
        return Objects.nonNull(request.getHeader("Origin"));
    }

}
