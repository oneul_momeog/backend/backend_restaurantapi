package oneulmomeog.socket.common.interceptor;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.MalformedJwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import oneulmomeog.socket.auth.jwt.JwtUtil;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.SignatureException;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Component
public class UserLoginInterceptor implements HandlerInterceptor {

    private final JwtUtil jwtUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("[Interceptor] UserLoginInterceptor");

        String bearerJwt = request.getHeader(HttpHeaders.AUTHORIZATION);
        log.info("BearerJwt = {}", bearerJwt);

        if (isPreflightRequest(request)) {
            return true;
        }

        String jwt = jwtUtil.getToken(bearerJwt);
        log.info("getToken = {}", jwt);

        Claims parseJwt = null;
        try {
            parseJwt = jwtUtil.parseJwt(jwt);
            log.info("parseJwt = {}", parseJwt);
        } catch (Exception e) {
            log.error(e.getMessage());
        }

        // TODO: 2022/11/21 채팅에서는 로그인을 따로 진행하지 않기 때문에 parsing을 제대로 하지 못한다. 이후에 api gateway를 이용하자.
        String userId = String.valueOf(parseJwt.get("userId"));
        String nickname = String.valueOf(parseJwt.get("nickname"));
        String zipcode = String.valueOf(parseJwt.get("zipcode"));

        request.setAttribute("id", userId);
        request.setAttribute("nickname", nickname);
        request.setAttribute("zipcode", zipcode);

//        request.setAttribute("id", 1);
//        request.setAttribute("nickname", "user1");
//        request.setAttribute("zipcode", "10323");
        return Boolean.TRUE;
    }

    private boolean isPreflightRequest(HttpServletRequest request) {
        return isOptions(request) && hasHeaders(request) && hasMethod(request) && hasOrigin(request);
    }

    private boolean isOptions(HttpServletRequest request) {
        return request.getMethod().equalsIgnoreCase(HttpMethod.OPTIONS.toString());
    }

    private boolean hasHeaders(HttpServletRequest request) {
        return Objects.nonNull(request.getHeader("Access-Control-Request-Headers"));
    }

    private boolean hasMethod(HttpServletRequest request) {
        return Objects.nonNull(request.getHeader("Access-Control-Request-Method"));
    }

    private boolean hasOrigin(HttpServletRequest request) {
        return Objects.nonNull(request.getHeader("Origin"));
    }

}
