package oneulmomeog.socket.common.file;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Component
@Slf4j
public class FileStore {

    @Value("${image.menu.fileDir}")
    private String reviewFileDir;

    // 파일을 저장하는 메서드
    public String storeFile(MultipartFile multipartFile) throws IOException {
        String originalFilename = multipartFile.getOriginalFilename();
        String storeFileName = changeFileNameToStore(originalFilename);
        String fullPath = getFullPath(storeFileName);

        multipartFile.transferTo(new File(fullPath));

        return fullPath;
    }


    // 파일 이름을 변경하는 메서드
    private String changeFileNameToStore(String originalFilename) {
        int loc = originalFilename.lastIndexOf(".");
        String ext = originalFilename.substring(loc + 1);

        String uuid = UUID.randomUUID().toString();
        return uuid + "." + ext;
    }


    // 파일 경로와 이름까지 한번에 불러와주는 메서드(src 속성에 들어갈 주소값)
    private String getFullPath(String filename) {
        return reviewFileDir + filename;
    }

    // 파일을 삭제하는 메서드
    public void deleteFile(String fullPath) {
        File file = new File(fullPath);
        if (file.exists()) {
            log.info("delete file : {}", fullPath);
            file.delete();
        }
    }

}
