package oneulmomeog.socket.user.domain;

import lombok.Getter;
import oneulmomeog.socket.common.domain.Generate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
@Getter
public class User extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<UserAddress> userAddressList = new ArrayList<>();

    @Column(nullable = false)
    private String nickname;

    // TODO: 2022/10/29 카카오와 네이버 2개로 회원가입 한 경우 email이 겹칠 수 있기 때문에 처리해 주어야 한다.
    @Column(unique = true)
    private String email;

    @Enumerated(EnumType.STRING)
    private LocalStatus localStatus;

    protected User() {}

    public User(String userId, String nickname, LocalStatus localStatus) {
        this.userId = userId;
        this.nickname = nickname;
        this.localStatus = localStatus;
    }

    public User(String userId, String nickname, LocalStatus localStatus, String email) {
        this.userId = userId;
        this.nickname = nickname;
        this.localStatus = localStatus;
        this.email = email;
    }
}
