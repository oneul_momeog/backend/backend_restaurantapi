package oneulmomeog.socket.user.domain;

import lombok.Getter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@Entity
@DynamicInsert
@Getter
public class UserAddress {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_address_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(nullable = false)
    private String zipcode;

    @Column(nullable = false)
    private String normalAddress;

    @Column(nullable = false)
    private String specificAddress;

    @ColumnDefault("true")
    private Boolean basic;

    protected UserAddress() {}

    public UserAddress(User user, String zipcode, String normalAddress, String specificAddress) {
        this.user = user;
        this.zipcode = zipcode;
        this.normalAddress = normalAddress;
        this.specificAddress = specificAddress;
    }
}
