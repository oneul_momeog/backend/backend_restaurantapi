package oneulmomeog.socket.user.repository;

import lombok.RequiredArgsConstructor;
import oneulmomeog.socket.user.domain.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
@RequiredArgsConstructor
public class UserRepository {

    private final EntityManager em;

    public User findById(Long id) {
        User user = em.find(User.class, id);
        return user;
    }
}
