package oneulmomeog.socket.room.repository;

import lombok.RequiredArgsConstructor;
import oneulmomeog.socket.room.domain.Room;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class RoomRepository {

    private final EntityManager em;

    public List<Room> getRooms(Long restaurantId) {
        List<Room> rooms = em.createQuery("select r from Room r" +
                        " join fetch r.roomAddress ra" +
                        " where r.restaurant.id = :restaurantId" +
                        " and r.deleted = false" +
                        " and r.status <> 'CREATE'" +
                        " and r.status <> 'FINISH'", Room.class)
                .setParameter("restaurantId", restaurantId)
                .getResultList();

        return rooms;
    }

    public List<Room> findRoomsByUserId(Long userId) {
        List<Room> rooms = em.createQuery("select distinct r from Room r" +
                        " join fetch r.restaurant re" +
                        " join fetch r.orders o" +
                        " where o.user.id = :userId" +
                        " and r.deleted = false", Room.class)
                .setParameter("userId", userId)
                .getResultList();

        return rooms;
    }

    public Room findRoomById(Long restaurantId, Long roomId) {
        Room room = em.createQuery("select r from Room r" +
                        " where r.restaurant.id = :restaurantId" +
                        " and r.id = :roomId", Room.class)
                .setParameter("restaurantId", restaurantId)
                .setParameter("roomId", roomId)
                .getResultStream()
                .findFirst()
                .orElse(null);
        return room;
    }

}
