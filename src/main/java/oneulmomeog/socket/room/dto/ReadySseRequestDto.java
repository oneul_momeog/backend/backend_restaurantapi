package oneulmomeog.socket.room.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ReadySseRequestDto {

    private Long restaurantId;
    private Long roomId;
}
