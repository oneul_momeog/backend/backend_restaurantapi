package oneulmomeog.socket.room.domain;

import lombok.Getter;
import oneulmomeog.socket.common.domain.Generate;
import oneulmomeog.socket.restaurant.domain.Restaurant;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@DynamicInsert
@Getter
public class Room extends Generate {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "room_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "room_address_id")
    private RoomAddress roomAddress;

    @Column(nullable = false)
    private String roomName;

    @Column(nullable = false)
    private String exMenu;

    private Integer maxPeople;
    private Integer currentPeople;

    @Column(nullable = false)
    @ColumnDefault("0")
    private int totalPrice;

    private Integer timer;

    @Enumerated(EnumType.STRING)
    @ColumnDefault("'CREATE'")
    private RoomStatus status;

    @OneToMany(mappedBy = "room")
    private List<Orders> orders = new ArrayList<>();


    // 생성자 메서드
    protected Room() {}

    public Room(Restaurant restaurant, RoomAddress roomAddress, String roomName, String exMenu, Integer maxPeople, Integer currentPeople, Integer totalPrice,  Integer timer) {
        this.restaurant = restaurant;
        this.roomAddress = roomAddress;
        this.roomName = roomName;
        this.exMenu = exMenu;
        this.maxPeople = maxPeople;
        this.currentPeople = currentPeople;
        this.totalPrice = totalPrice;
        this.timer = timer;
    }

    /* 비즈니스 메서드 */
    public void updateRoomStatus(RoomStatus roomStatus) {
        if (roomStatus.equals(RoomStatus.READY) && this.status.equals(RoomStatus.CREATE)) {
            this.status = roomStatus;
        } else if (roomStatus.equals(RoomStatus.RECEIVE) && this.status.equals(RoomStatus.READY)) {
            this.status = roomStatus;
        } else if (roomStatus.equals(RoomStatus.DELIVERY) && this.status.equals(RoomStatus.RECEIVE)) {
            this.status = roomStatus;
        } else if (roomStatus.equals(RoomStatus.FINISH) && this.status.equals(RoomStatus.DELIVERY)) {
            this.status = roomStatus;
        } else if (roomStatus.equals(RoomStatus.CANCEL)) {
            this.status = roomStatus;
        } else {
            throw new IllegalArgumentException("룸 상태를 변경할 수 없습니다.");
        }
    }

    public void deleteRoom() {
        this.delete();
    }
}






















