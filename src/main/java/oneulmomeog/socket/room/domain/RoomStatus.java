package oneulmomeog.socket.room.domain;

public enum RoomStatus {
    CREATE, READY, RECEIVE, DELIVERY, FINISH, CANCEL
}
