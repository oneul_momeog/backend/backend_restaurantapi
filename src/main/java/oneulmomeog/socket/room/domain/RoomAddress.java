package oneulmomeog.socket.room.domain;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Getter
public class RoomAddress {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "room_address_id")
    public Long id;

    @Column(nullable = false)
    public String zipcode;

    @Column(nullable = false)
    public String normalAddress;

    @Column(nullable = false)
    public String specificAddress;

    // 생성자 메서드
    protected RoomAddress() {}

    public RoomAddress(String zipcode, String normalAddress, String specificAddress) {
        this.zipcode = zipcode;
        this.normalAddress = normalAddress;
        this.specificAddress = specificAddress;
    }

    public void changeRoomAddress(String zipcode, String normalAddress, String specificAddress) {
        this.zipcode = zipcode;
        this.normalAddress = normalAddress;
        this.specificAddress = specificAddress;
    }

    public String getFullAddress() {
        return this.normalAddress + " " + this.specificAddress;
    }
}
